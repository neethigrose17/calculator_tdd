package org.example;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class MainTest {

    @Test
    void checkAddition() {
        Calculator calc = new Calculator();
        int result = calc.add(2,2);
        assertEquals(4,result);
        assertNotEquals(5, result);
    }

    @Test
    void checkSubtraction() {
        Calculator calc = new Calculator();
        int result = calc.subtract(4,2);
        assertEquals(2,result);
        assertNotEquals(3, result);
    }

    @Test
    void checkMultiplication() {
        Calculator calc = new Calculator();
        int result = calc.multiply(3,2);
        assertEquals(6,result);
        assertNotEquals(7, result);
    }
    @Test
    void checkDivision() {
        Calculator calc = new Calculator();
        double result = calc.divide(4,2);
        assertEquals(2,result);
        assertNotEquals(3, result);
    }

    @Test
    void cannotDivideByZero() {
        Calculator calc = new Calculator();
        double result = calc.divide(4,0);
        assertEquals(Double.POSITIVE_INFINITY, result);
    }

    @Test
    void divisionByZeroWillStillAddToHistory() {
        Calculator calc = new Calculator();
        calc.divide(4,0);
        ArrayList<String> result = calc.history();
        ArrayList<String> expectedArray = new ArrayList<String>();
        expectedArray.add("4 / 0 = undefined");
        assertEquals(expectedArray, result);
    }

    @Test
    void checkShowHistory() {
        Calculator calc = new Calculator();
        calc.add(2,2);
        calc.subtract(4,2);
        calc.multiply(3,2);
        calc.divide(4,2);
        ArrayList<String> result = calc.history();
        ArrayList<String> expectedArray = new ArrayList<String>();
        expectedArray.add("2 + 2 = 4");
        expectedArray.add("4 - 2 = 2");
        expectedArray.add("3 * 2 = 6");
        expectedArray.add("4 / 2 = 2");
        assertEquals(expectedArray, result);
    }
}
