package org.example;

import java.util.ArrayList;

public class Calculator {
    private ArrayList<String> history = new ArrayList<String>();
    public int add(int i, int i1) {
        int sum = i + i1;
        history.add(String.format("%d + %d = %d", i, i1, sum));
        return sum;
    }

    public int subtract(int i, int i1) {
        int difference = i - i1;
        history.add(String.format("%d - %d = %d", i, i1, difference));
        return difference;
    }

    public int multiply(int i, int i1) {
        int product = i * i1;
        history.add(String.format("%d * %d = %d", i, i1, product));
        return product;
    }

    public double divide(int i, int i1) {
        double quotient;
        if (i1 == 0) {
            quotient = Double.POSITIVE_INFINITY;
            history.add(String.format("%d / %d = undefined", i, i1));
        } else {
            quotient = i / i1;
            history.add(String.format("%d / %d = %d", i, i1, quotient));
        }
        return quotient;
    }

    public ArrayList<String> history() {
        return history;
    }
}
